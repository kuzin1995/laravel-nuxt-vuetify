import Vue from 'vue'
// import { HasError, AlertError, AlertSuccess } from 'vform'
// Заменили на свои с разметкой Vuetify. HasError не нужен, т.к. ошибки передаются параметром в v-text-field
import AlertError from '~/components/vform-alert/AlertError'
import AlertErrors from '~/components/vform-alert/AlertErrors'
import AlertSuccess from '~/components/vform-alert/AlertSuccess'

Vue.component(AlertError.name, AlertError)
Vue.component(AlertErrors.name, AlertErrors)
Vue.component(AlertSuccess.name, AlertSuccess)
