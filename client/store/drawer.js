import Cookies from 'js-cookie'

// state
export const state = () => ({
  drawer: null
})

// getters
export const getters = {
  drawer: state => state.drawer
}

// mutations
export const mutations = {
  SET_DRAWER (state, { drawer }) {
    drawer === 'false' ? state.drawer = false : state.drawer = drawer
  }
}

// actions
export const actions = {
  toggleDrawer ({ commit, state }) {
    let drawer = !state.drawer
    commit('SET_DRAWER', { drawer })

    Cookies.set('drawer', drawer, { expires: 365 })
  }
}
