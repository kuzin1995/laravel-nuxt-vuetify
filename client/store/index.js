import Cookies from 'js-cookie'
import {cookieFromRequest} from '~/utils'

export const actions = {
  nuxtServerInit({commit}, {req}) {
    const token = cookieFromRequest(req, 'token')
    if (token) {
      commit('auth/SET_TOKEN', token)
    }

    const locale = cookieFromRequest(req, 'locale')
    if (locale) {
      commit('lang/SET_LOCALE', { locale })
    }

    const drawer = cookieFromRequest(req, 'drawer')
    if (drawer) {
      commit('drawer/SET_DRAWER', { drawer })
    }
  },

  nuxtClientInit({commit}) {
    const token = Cookies.get('token')
    if (token) {
      commit('auth/SET_TOKEN', token)
    }

    const locale = Cookies.get('locale')
    if (locale) {
      commit('lang/SET_LOCALE', { locale })
    }

    const drawer = Cookies.get('drawer')
    if (drawer) {
      commit('drawer/SET_DRAWER', { drawer })
    }
  }
}
