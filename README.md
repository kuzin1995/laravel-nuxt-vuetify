# laravel-nuxt-vuetify

> My Laravel-Nuxt project based on out [Laravel-Nuxt by cretueusebiu](https://github.com/cretueusebiu/laravel-nuxt) and [Vuetify module](https://github.com/nuxt-community/vuetify-module).

## Build Setup

``` bash
# install dependencies
$ yarn run install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn run start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org), [Vuetify docs](https://vuetifyjs.com/ru/getting-started/quick-start) and [Laravel docs](https://laravel.com/docs).
